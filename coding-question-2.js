
// Problem 2: Complete the calculateFrequency function that takes lowercase string as input and returns frequency of all english alphabet. (using only array, no in-built function)
function calculateFrequency(string) {
    let frequency = {}
    for(let i=0;i<string.length;i++)
    {
        let tempChar=string.charAt(i);
        let charAscii=string.charCodeAt(i);
        if((charAscii-97)>26 || (charAscii-97)<0)
        {
            
        }else{
            frequency[tempChar]=0;
        }
    }
    for(let i=0;i<string.length;i++)
    {
        let tempChar=string.charAt(i);
        let charAscii=string.charCodeAt(i);
        if((charAscii-97)>26 || (charAscii-97)<0)
        {
            
        }else{
            let tempValue=frequency[tempChar];
            tempValue++;
            frequency[tempChar]=tempValue;
        }
    }
      return frequency;
    }
