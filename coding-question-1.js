// Problem 1: Complete the secondLargest function which takes in an array of numbers in input and return the second biggest number in the array. (without using sort)?
function secondLargest(array) {
    let secondLargest=0;
    let firstLargest=0;
    for(let i=0;i<array.length;i++)
      {
        if(array[i]>firstLargest)
          {
            secondLargest=firstLargest;
            firstLargest=array[i];
          }else
            {
              if(array[i]>secondLargest && array[i]!=firstLargest){
                secondLargest=array[i];
              }
            }
      }
    return secondLargest;
  }
  
