function flatten(unflattenObject)
{
    let flatObj = {};
    for (let key in unflattenObject)
    {
        if ((typeof unflattenObject[key]) == 'object')
        {
            var tempFlatten = flatten(unflattenObject[key]);
            for (let tempKey in tempFlatten) 
             {
                 flatObj[key+'.'+ tempKey]=tempFlatten[tempKey];
             }
        }else
        {
            flatObj[key] = unflattenObject[key];
        }
    }
    return flatObj; 
}
